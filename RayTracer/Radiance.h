#ifndef __RADIANCE_H__
#define __RADIANCE_H__

#include <iostream>


/**
 * @class  Radiance
 * @author Sarathi Hansen
 *
 * A Radiance represents a physical radiance value with a red, green, and blue component.
 */

class Radiance
{
public:
	double r;
	double g;
	double b;
	
public:
	Radiance (double r, double g, double b);
	Radiance (double v);
	Radiance ();
	
	Radiance (const Radiance & other);
	
public:
	Radiance & operator = (const Radiance & other);
	
public:
	Radiance clamp (double minBound, double maxBound) const;
	
public:
	Radiance & operator += (const Radiance & other);
	Radiance & operator -= (const Radiance & other);
	Radiance & operator *= (const Radiance & other);
	Radiance & operator /= (const Radiance & other);
	
	Radiance & operator += (double factor);
	Radiance & operator -= (double factor);
	Radiance & operator *= (double factor);
	Radiance & operator /= (double factor);
	
	Radiance operator + (const Radiance & other) const;
	Radiance operator - (const Radiance & other) const;
	Radiance operator * (const Radiance & other) const;
	Radiance operator / (const Radiance & other) const;
	
	Radiance operator + (double factor) const;
	Radiance operator - (double factor) const;
	Radiance operator * (double factor) const;
	Radiance operator / (double factor) const;
	
private:
	friend Radiance operator + (double factor, const Radiance & radiance);
	friend Radiance operator * (double factor, const Radiance & radiance);
	
private:
	friend std::ostream & operator << (std::ostream & os, const Radiance & radiance);
};

#endif // __RADIANCE_H__
