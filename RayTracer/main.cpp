#include "RayTracer.h"


/**
 * @author Sarathi Hansen
 *
 * The main function just runs the RayTracer application class.
 */

int main (int argc, const char * argv[])
{
	new RayTracer ();
	
    return 0;
}
