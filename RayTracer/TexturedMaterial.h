#ifndef __TEXTURED_MATERIAL_H__
#define __TEXTURED_MATERIAL_H__

#include "Material.h"
#include "Radiance.h"
#include "bitmap_image.hpp"
#include <string>

using namespace std;

/**
 * @class  TexturedMaterial
 * @author Sarathi Hansen
 *
 * Represents a material that maps a texture from a file to the object.
 */

class TexturedMaterial : public Material
{
private:
	bitmap_image m_image;
	
public:
	TexturedMaterial (const string & textureFile,
					  double diffuse = 1.0,
					  double specular = 0.0,
					  double specularExponent = 0.0,
					  double reflectance = 0.0,
					  double transmittance = 0.0,
					  double indexOfRefraction = 1.0):
	Material (diffuse, specular, specularExponent, reflectance, transmittance, indexOfRefraction),
	m_image (textureFile) {}
	
	void operator = (const TexturedMaterial & other)
	{
		diffuse = other.diffuse;
		specular = other.specular;
		specularExponent = other.specularExponent;
		m_image = bitmap_image (other.m_image);
		reflectance = other.reflectance;
		transmittance = other.transmittance;
	}
	
public:
	virtual Radiance diffuseColor (double u, double v)
	{
		unsigned int x = m_image.width () * u;
		unsigned int y = m_image.height () - m_image.height () * v;
		
		unsigned char r;
		unsigned char g;
		unsigned char b;
		
		m_image.get_pixel (x, y, r, g, b);
		
		return Radiance (r/255.0, g/255.0, b/255.0);
	}
	
	virtual Radiance specularColor (double /*u*/, double /*v*/)
	{
		return Radiance (1.0, 1.0, 1.0);
	}
};

#endif // __TEXTURED_MATERIAL_H__
