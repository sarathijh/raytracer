#include "Radiance.h"
#include <algorithm>

using namespace std;


Radiance::Radiance (double r, double g, double b):
r (r),
g (g),
b (b)
{
	
}

Radiance::Radiance (double v):
Radiance (v, v, v)
{
	
}

Radiance::Radiance ():
Radiance (0, 0, 0)
{
	
}

Radiance::Radiance (const Radiance & other):
r (other.r),
g (other.g),
b (other.b)
{
	
}

Radiance & Radiance::operator = (const Radiance & other)
{
	r = other.r;
	g = other.g;
	b = other.b;
	
	return *this;
}

Radiance Radiance::clamp (double minBound, double maxBound) const
{
	Radiance copy (*this);
	
	copy.r = min (max (copy.r, minBound), maxBound);
	copy.g = min (max (copy.g, minBound), maxBound);
	copy.b = min (max (copy.b, minBound), maxBound);
	
	return copy;
}

Radiance & Radiance::operator += (const Radiance & other)
{
	r += other.r;
	g += other.g;
	b += other.b;
	
	return *this;
}
Radiance & Radiance::operator -= (const Radiance & other)
{
	r -= other.r;
	g -= other.g;
	b -= other.b;
	
	return *this;
}
Radiance & Radiance::operator *= (const Radiance & other)
{
	r *= other.r;
	g *= other.g;
	b *= other.b;
	
	return *this;
}
Radiance & Radiance::operator /= (const Radiance & other)
{
	r /= other.r;
	g /= other.g;
	b /= other.b;
	
	return *this;
}

Radiance & Radiance::operator += (double factor)
{
	r += factor;
	g += factor;
	b += factor;
	
	return *this;
}
Radiance & Radiance::operator -= (double factor)
{
	r -= factor;
	g -= factor;
	b -= factor;
	
	return *this;
}
Radiance & Radiance::operator *= (double factor)
{
	r *= factor;
	g *= factor;
	b *= factor;
	
	return *this;
}
Radiance & Radiance::operator /= (double factor)
{
	r /= factor;
	g /= factor;
	b /= factor;
	
	return *this;
}

Radiance Radiance::operator + (const Radiance & other) const
{
	Radiance result (*this);
	result += other;
	
	return result;
}
Radiance Radiance::operator - (const Radiance & other) const
{
	Radiance result (*this);
	result -= other;
	
	return result;
}
Radiance Radiance::operator * (const Radiance & other) const
{
	Radiance result (*this);
	result *= other;
	
	return result;
}
Radiance Radiance::operator / (const Radiance & other) const
{
	Radiance result (*this);
	result /= other;
	
	return result;
}

Radiance Radiance::operator + (double factor) const
{
	Radiance result (*this);
	result += factor;
	
	return result;
}
Radiance Radiance::operator - (double factor) const
{
	Radiance result (*this);
	result -= factor;
	
	return result;
}
Radiance Radiance::operator * (double factor) const
{
	Radiance result (*this);
	result *= factor;
	
	return result;
}
Radiance Radiance::operator / (double factor) const
{
	Radiance result (*this);
	result /= factor;
	
	return result;
}


Radiance operator + (double factor, const Radiance & radiance)
{
	return radiance + factor;
}
Radiance operator * (double factor, const Radiance & radiance)
{
	return radiance * factor;
}

std::ostream & operator << (std::ostream & os, const Radiance & radiance)
{
    os << "Radiance (" << radiance.r << " " << radiance.g << " " << radiance.b << ")";
    return os;
}
