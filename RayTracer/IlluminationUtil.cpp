#include "IlluminationUtil.h"


Radiance IlluminationUtil::illuminate (const IntersectionData3D * intersectionData,
									   const Scene3D * scene,
									   IlluminationUtil::model illuminationModel)
{
	Radiance radiance;
	
	// Loop through each light and sum each of the radiances that illuminate the intersection point.
	for (Light3D * light : scene->lights ())
	{
		// Calculate a "shadow ray" from the point of intersection to the light source.
		Ray3D shadowRay (light->position () - intersectionData->position,
						 intersectionData->position,
						 1.0);
		
		// If the shadow ray doesn't intersect any objects before it hits the light source,
		// then run the given illumination model's logic for illuminating the point.
		if (scene->rayHitsLight (shadowRay, light))
			radiance += illuminationModel (intersectionData, light);
	}
	
	// Return the radiance scaled down by the intensity of the incoming ray.
	return radiance;
}

Radiance IlluminationUtil::flatModel (const IntersectionData3D * intersectionData,
									  const Light3D * light)
{
	Material * material = intersectionData->object->material ();
	return material->diffuseColor (intersectionData->u, intersectionData->v);
}

Radiance IlluminationUtil::phongModel (const IntersectionData3D * intersectionData,
									   const Light3D * light)
{
	vec3 N = normalise (intersectionData->normal);
	vec3 L = normalise (light->position () - intersectionData->position);
	vec3 E = normalise (intersectionData->position);
	
	bool inside = (dot (N, L) < 0);
	if (inside)
		N = -N;
	
	vec3 R = normalise (L - 2 * dot (L, N) * N);
	
	Material * material = intersectionData->object->material ();
	
	Radiance materialDiffuse = material->diffuseColor (intersectionData->u, intersectionData->v);
	Radiance materialSpecular = material->specularColor (intersectionData->u, intersectionData->v);
	
	Radiance diffuse = light->radiance * materialDiffuse * max (dot (N, L), 0.0);
	Radiance specular = light->radiance * materialSpecular * pow (max (dot (R, E), 0.0), material->specularExponent);
	
	return diffuse * material->diffuse + specular * material->specular;
}
