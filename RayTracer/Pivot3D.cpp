#include "Pivot3D.h"
#include "TransformUtil.h"

using namespace arma;


Pivot3D::Pivot3D (const vec3 & position):
m_position (position)
{
	
}

void Pivot3D::transform (const mat44 & matrix)
{
	m_position = TransformUtil::transformVector (m_position, matrix);
}
