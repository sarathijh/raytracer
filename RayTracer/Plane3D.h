#ifndef __RayTracer__Plane3D__
#define __RayTracer__Plane3D__

#include "Object3D.h"
#include "Triangle3D.h"

using namespace arma;


/**
 * @class 	Plane3D
 * @author 	Sarathi Hansen
 *
 * The Plane3D class defines the geometry for a 3D plane
 * to be transformed and placed into a Scene3D.
 */

class Plane3D : public Object3D
{
private:
	Triangle3D * m_t1;
	Triangle3D * m_t2;
	
public:
	Plane3D (const vec3 & position,
			 double width,
			 double height,
			 const vec3 & normal,
			 Material * material):
	Object3D (position, material)
	{
		vec3 n = normalise (normal);
		vec3 u = normalise (cross (vec3 {0.0, 1.0, 0.0}, n));
		
		if (sum (abs (u)) == 0)
			u = vec3 {1, 0, 0};
		
		vec3 w = normalise (cross (n, u));
		
		u *= width / 2.0;
		w *= height / 2.0;
		
		vec3 tl = position + u + w;
		vec3 tr = position - u + w;
		vec3 bl = position + u - w;
		vec3 br = position - u - w;
		
		m_t1 = new Triangle3D (tl, tr, bl, material);
		m_t2 = new Triangle3D (br, bl, tr, material);
	}
	
	void transform (const mat44 & matrix)
	{
		Object3D::transform (matrix);
		
		m_t1->transform (matrix);
		m_t2->transform (matrix);
	}
	
	virtual const IntersectionData3D * rayIntersection (const Ray3D & ray, bool print = false) const
	{
		const IntersectionData3D * i1 = m_t1->rayIntersection (ray);
		const IntersectionData3D * i2 = m_t2->rayIntersection (ray);
		
		if (i1 == NULL && i2 == NULL)
			return NULL;
		else if (i1 == NULL)
			return i2;
		else if (i2 == NULL)
			return i1;
		
		return i1->omega < i2->omega ? i1 : i2;
	}
};

#endif /* defined(__RayTracer__Plane3D__) */
