#include "Object3D.h"


double Object3D::OMEGA_EPSILON = 0.0001;


Object3D::Object3D (const vec3 & position, Material * material):
Pivot3D (position),
m_material (material)
{
	
}

// STATIC

bool Object3D::isOmegaValid (double omega)
{
	return (omega >= Object3D::OMEGA_EPSILON);
}

double Object3D::findSmallestOmega (const std::vector<double> & omegas)
{
	double min = std::numeric_limits<double>::max ();
	
	for (double omega : omegas)
	{
		if (isOmegaValid (omega) && omega < min)
			min = omega;
	}
	
	return min;
}

// DEBUG

std::ostream & operator << (std::ostream & os, const Object3D * object)
{
    os << object->position ();
    return os;
}
