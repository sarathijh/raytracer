#ifndef __TONE_REPRODUCTION_UTIL_H__
#define __TONE_REPRODUCTION_UTIL_H__

#include <functional>
#include <vector>

#include "Radiance.h"

using namespace std;


/**
 * @class	ToneReproductionUtil
 * @author	Sarathi Hansen
 *
 * The ToneReproductionUtil class supplies static methods for applying different
 * tone reproduction operators for a list of pixel radiance values.
 */

class ToneReproductionUtil
{
public:
	typedef std::function<void (vector<Radiance> &)> model;
	
public:
	static double getAbsoluteLuminance (const Radiance & radiance);
	static double getLogAverageLuminance (const vector<Radiance> & pixelRadianceValues);
	static double getMaxLuminance (const vector<Radiance> & pixelRadianceValues);
	static void modelColorAppearance (Radiance & radiance, double ldmax);
	
public:
	static model linearModel (double ldmax = 86);
	static model wardModel (double ldmax = 86);
	static model reinhardModel (double ldmax = 86, double key = 0.18);
	static model adaptiveLogarithmicModel (double ldmax = 86, double b = 0.85);
};

#endif // __TONE_REPRODUCTION_UTIL_H__
