#ifndef __CHECKERED_MATERIAL_H__
#define __CHECKERED_MATERIAL_H__

#include "Material.h"
#include "Radiance.h"

/**
 * @class  CheckeredMaterial
 * @author Sarathi Hansen
 *
 * Represents a material with a checker board pattern.
 */

class CheckeredMaterial : public Material
{
private:
	Radiance m_color1;
	Radiance m_color2;
	int m_horizontal;
	int m_vertical;
	
public:
	CheckeredMaterial (const Radiance & color1,
					   const Radiance & color2,
					   int horizontal,
					   int vertical,
					   double diffuse = 1.0,
					   double specular = 0.0,
					   double specularExponent = 0.0,
					   double reflectance = 0.0,
					   double transmittance = 0.0,
					   double indexOfRefraction = 1.0):
	Material (diffuse, specular, specularExponent, reflectance, transmittance, indexOfRefraction),
	m_color1 (color1),
	m_color2 (color2),
	m_horizontal (horizontal),
	m_vertical (vertical)
	{}
	
	void operator = (const CheckeredMaterial & other)
	{
		m_color1 = other.m_color1;
		m_color2 = other.m_color2;
		m_horizontal = other.m_horizontal;
		m_vertical = other.m_vertical;
		diffuse = other.diffuse;
		specular = other.specular;
		specularExponent = other.specularExponent;
		reflectance = other.reflectance;
		transmittance = other.transmittance;
	}
	
public:
	virtual Radiance diffuseColor (double u, double v)
	{
		u *= m_horizontal;
		v *= m_vertical;
		
		if ((int)floor (u) % 2 == (int)floor (v) % 2)
			return m_color1;
		else
			return m_color2;
	}
	
	virtual Radiance specularColor (double /*u*/, double /*v*/)
	{
		return Radiance (1.0, 1.0, 1.0);
	}
};

#endif // __CHECKERED_MATERIAL_H__
