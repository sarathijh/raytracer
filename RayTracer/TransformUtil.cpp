#include "TransformUtil.h"


/**
 * In order to transform a vector, it needs to be given a homogenous fourth coordinate.
 * Therefore, this coordinate is added, the vector is transformed, then the homogenous
 * coordinate is removed.
 */
vec3 TransformUtil::transformVector (const vec3 & vector, const mat44 & matrix)
{
	vec4 homogenous = {vector(0), vector(1), vector(2), 1.0};
	vec4 transformed = matrix * homogenous;
	
	return vec3 {transformed(0), transformed(1), transformed(2)};
}
