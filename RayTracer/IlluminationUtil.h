#ifndef __ILLUMINATIONMODEL3D_H__
#define __ILLUMINATIONMODEL3D_H__

#include "Scene3D.h"
#include "Light3D.h"
#include "Radiance.h"
#include "IntersectionData3D.h"

using namespace arma;

/**
 * @class 	IlluminationModel3D
 * @author 	Sarathi Hansen
 *
 * The IlluminationUtil class supplies static methods for applying
 * illumination models to a single intersection in a scene.
 */

class IlluminationUtil
{
public:
	typedef std::function<Radiance (const IntersectionData3D *, const Light3D *)> model;
	
	
public:
	static Radiance illuminate (const IntersectionData3D * intersectionData,
								const Scene3D * scene,
								IlluminationUtil::model illuminationModel);
	
public:
	static Radiance flatModel (const IntersectionData3D * intersectionData,
							   const Light3D * light);
	
	static Radiance phongModel (const IntersectionData3D * intersectionData,
								const Light3D * light);
};

#endif // __ILLUMINATIONMODEL3D_H__
