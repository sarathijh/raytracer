#ifndef __TRANSFORM_UTIL_H__
#define __TRANSFORM_UTIL_H__

#include <armadillo>

using namespace arma;


/**
 * @class 	TransformUtil
 * @author 	Sarathi Hansen
 *
 * Includes helper methods for performing vector/matrix transformations.
 */

class TransformUtil
{
public:
	/**
	 * Transforms the given 3d vector using the given transformation matrix.
	 */
	static vec3 transformVector (const vec3 & vector, const mat44 & matrix);
};

#endif // __TRANSFORM_UTIL_H__
