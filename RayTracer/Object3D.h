#ifndef __OBJECT_3D_H__
#define __OBJECT_3D_H__

#include <iostream>
#include <armadillo>

#include "Pivot3D.h"
#include "Material.h"
#include "Ray3D.h"

using namespace arma;


class IntersectionData3D;


/**
 * @class	Object3D
 * @author	Sarathi Hansen
 *
 * The Object3D class represents an object that can be transformed
 * and placed into a Scene3D.
 */
class Object3D : public Pivot3D
{
public:
	static double OMEGA_EPSILON;
	
private:
	Material * m_material;
	
public:
	Material * material () const { return m_material; }
	void setMaterial (Material * value) { m_material = value; }
	
public:
	Object3D (const vec3 & position, Material * material);
	
public:
	virtual const IntersectionData3D * rayIntersection (const Ray3D &, bool print = false) const = 0;
	
public:
	static bool isOmegaValid (double omega);
	static double findSmallestOmega (const std::vector<double> & omegas);
	
private:
	friend std::ostream & operator << (std::ostream & os, const Object3D * object);
};

#endif // __OBJECT_3D_H__
