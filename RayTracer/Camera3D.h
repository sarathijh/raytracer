#ifndef __CAMERA_3D_H__
#define __CAMERA_3D_H__

#include "Scene3D.h"
#include "Ray3D.h"
#include "IlluminationUtil.h"
#include "SamplingUtil.h"
#include "ToneReproductionUtil.h"
#include "bitmap_image.hpp"

#include <string>

using namespace std;
using namespace arma;


/**
 * @class  Camera3D
 * @author Sarathi Hansen
 *
 * The Camera3D class represents an independent camera
 * that can be used to render a scene.
 */

class Camera3D
{
public:
	/// A shader is a function that takes an intersection data and returns the radiance value at that intersection
	typedef std::function<Radiance (const IntersectionData3D *)> shader;
	
public:
	/// The number of threads to spawn; set to the concurrency capacity of the hardware
	static const unsigned int nthreads;
	
public:
	/**
	 * @struct ViewPlane
	 * @author Sarathi Hansen
	 *
	 * Contains information about the camera's view plane.
	 */
	struct ViewPlane
	{
		int width;
		int height;
		double z;
	};
	
private:
	vec3 m_position;
	vec3 m_lookat;
	vec3 m_up;
	Camera3D::ViewPlane m_viewPlane;
	
	mat44 m_viewMatrix;
	bool m_viewMatrixDirty;
	
	// GETTERS and SETTERS
public:
	const vec3      & position  () { return m_position; }
	const vec3      & lookat    () { return m_lookat; }
	const vec3      & up        () { return m_up; }
	const ViewPlane & viewPlane () { return m_viewPlane; }
	
	void setPosition (const vec3 & v) { m_position = v; m_viewMatrixDirty = true; }
	void setLookat (const vec3 & v) { m_lookat = v; m_viewMatrixDirty = true; }
	void setUp (const vec3 & v) { m_up = v; m_viewMatrixDirty = true; }
	void setViewPlane (const ViewPlane & v) { m_viewPlane = v; }
	
	// CONSTRUCTOR(S) and DESTRUCTOR
public:
	Camera3D (const vec3 & position,
			  const vec3 & lookat,
			  const vec3 & up,
			  const Camera3D::ViewPlane & viewPlane);
	
public:
	/**
	 * Returns the view matrix used to transform a point to camera space.
	 */
	mat44 viewMatrix ();
	
	/**
	 * Returns a ray in camera space from the origin to the given point on the view plane.
	 */
	Ray3D getRayThrough (double x, double y);
	
	/**
	 * Renders the given scene using the given illumination model, sampler, and tone reproduction model.
	 * The rendered image is saved to the given filename.
	 */
	void render (const string & filename,
				 Scene3D * scene,
				 IlluminationUtil::model illuminationModel = IlluminationUtil::phongModel,
				 ToneReproductionUtil::model toneReproductionModel = ToneReproductionUtil::linearModel (),
				 SamplingUtil::sampler sampler = SamplingUtil::center);
	
	/**
	 * Renders the given scene using the given shader, illumination model, and tone reproduction model.
	 * The rendered image is saved to the given filename.
	 */
	void render (const string & filename,
				 Scene3D * scene,
				 Camera3D::shader surfaceShader,
				 ToneReproductionUtil::model toneReproductionModel = ToneReproductionUtil::linearModel (),
				 SamplingUtil::sampler sampler = SamplingUtil::center);
};

#endif // __CAMERA_3D_H__
