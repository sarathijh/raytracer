#ifndef __RayTracer__Triangle3D__
#define __RayTracer__Triangle3D__

#include "Object3D.h"
#include "TransformUtil.h"

using namespace arma;


/**
 * @class 	Triangle3D
 * @author 	Sarathi Hansen
 *
 * The Triangle3D class defines the geometry for a 3D triangle
 * to be transformed and placed into a Scene3D.
 */

class Triangle3D : public Object3D
{
private:
	vec3 m_A;
	vec3 m_B;
	vec3 m_C;
	
public:
	Triangle3D (const vec3 & A,
				const vec3 & B,
				const vec3 & C,
				Material * material):
	Object3D (vec3 {0.0, 0.0, 0.0}, material),
	m_A (A),
	m_B (B),
	m_C (C) {}
	
	void transform (const mat44 & matrix)
	{
		Object3D::transform (matrix);
		
		m_A = TransformUtil::transformVector (m_A, matrix);
		m_B = TransformUtil::transformVector (m_B, matrix);
		m_C = TransformUtil::transformVector (m_C, matrix);
	}
	
	virtual const IntersectionData3D * rayIntersection (const Ray3D & ray, bool print = false) const
	{
		// Calculate the edge vectors of the triangle
		vec3 e1 = m_B - m_A;
		vec3 e2 = m_C - m_A;
		
		vec3 T = ray.position - m_A;
		vec3 P = cross (ray.direction, e2);
		vec3 Q = cross (T, e1);
		
		double t = dot (Q, e2) / dot (P, e1);
		double u = dot (P, T) / dot (P, e1);
		double v = dot (Q, ray.direction) / dot (P, e1);
		
		if (dot (P, e1) == 0 || (! Object3D::isOmegaValid (t)) || u < 0 || v < 0 || u+v > 1)
			return NULL;
		
		vec3 point = m_A + u*e1 + v*e2;
		vec3 normal = normalise (cross (e1, e2));
		
		return new IntersectionData3D (point, normal, ray, t, u, v, this);
	}
};

#endif /* defined(__RayTracer__Triangle3D__) */
