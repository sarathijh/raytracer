#ifndef __LIGHT_3D_H__
#define __LIGHT_3D_H__

#include <armadillo>

#include "Pivot3D.h"
#include "Radiance.h"

using namespace arma;


/**
 * @class 	Light3D
 * @author 	Sarathi Hansen
 *
 * The Light3D class represents a point light that can be transformed
 * and placed into a Scene3D.
 */

class Light3D : public Pivot3D
{
public:
	const Radiance radiance;
	
public:
	Light3D (const vec3 & position,
			 const Radiance & radiance):
	Pivot3D (position),
	radiance (radiance) {}
};

#endif // __LIGHT_3D_H__
