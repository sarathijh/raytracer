#ifndef __SAMPLING_UTIL_H__
#define __SAMPLING_UTIL_H__

#include <functional>
#include <vector>

#include "Radiance.h"

using namespace std;


/**
 * @class	SamplingUtil
 * @author	Sarathi Hansen
 *
 * The SamplingUtil class supplies static methods for applying
 * different sampling algorithms when rendering a scene.
 */

class SamplingUtil
{
public:
	typedef std::function<Radiance (std::function<Radiance (double, double)>)> sampler;
	
public:
	static Radiance center (std::function<Radiance (double, double)> callback);
	static sampler grid (double samples);
};

#endif // __SAMPLING_UTIL_H__
