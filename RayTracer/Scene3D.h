#ifndef __SCENE_3D_H__
#define __SCENE_3D_H__

#include <iostream>
#include <vector>

#include "Object3D.h"
#include "Light3D.h"
#include "Radiance.h"
#include "IntersectionData3D.h"

using namespace std;
using namespace arma;


/**
 * @class	Scene3D
 * @author	Sarathi Hansen
 *
 * The Scene3D class represents an independent 3D scene in which
 * 3D objects can be created and manipulated. Scene management
 * happens through the scene data structures, which are exposed
 * using the addObject and addLight methods.
 */
class Scene3D
{
private:
	vector<Object3D*> m_objects;
	vector<Light3D*> m_lights;
	
	Radiance m_backgroundRadiance;
	
public:
	const vector<Light3D*> & lights () const { return m_lights; }
	const Radiance & backgroundRadiance () const { return m_backgroundRadiance; }
	
public:
	Scene3D (const Radiance & backgroundRadiance);
	
public:
	void addObject (Object3D * object);
	void addLight (Light3D * light);
	
public:
	void transform (const mat44 & matrix);
	
public:
	bool rayHitsLight (const Ray3D & ray, Light3D * light) const;
	const IntersectionData3D * closestIntersectionWithRay (const Ray3D & ray) const;
	Radiance traceRay (const Ray3D & ray, int maxDepth, std::function<Radiance (const IntersectionData3D * intersectionData)> shader) const;
	
private:
	static Ray3D getReflectedRayFromIntersectionData (const IntersectionData3D * intersectionData);
	static Ray3D getTransmittedRayFromIntersectionData (const IntersectionData3D * intersectionData);
};

#endif // __SCENE_3D_H__
