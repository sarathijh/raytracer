#ifndef __COLOR_MATERIAL_H__
#define __COLOR_MATERIAL_H__

#include "Material.h"
#include "Radiance.h"

/**
 * @class  ColorMaterial
 * @author Sarathi Hansen
 *
 * Represents a material with explicit diffuse and specular colors.
 */

class ColorMaterial : public Material
{
private:
	Radiance m_diffuseColor;
	Radiance m_specularColor;
	
public:
	ColorMaterial (const Radiance & diffuseColor,
				   const Radiance & specularColor,
				   double diffuse = 1.0,
				   double specular = 0.0,
				   double specularExponent = 0.0,
				   double reflectance = 0.0,
				   double transmittance = 0.0,
				   double indexOfRefraction = 1.0):
	Material (diffuse, specular, specularExponent, reflectance, transmittance, indexOfRefraction),
	m_diffuseColor (diffuseColor),
	m_specularColor (specularColor) {}
	
	void operator = (const ColorMaterial & other)
	{
		m_diffuseColor = other.m_diffuseColor;
		m_specularColor = other.m_specularColor;
		diffuse = other.diffuse;
		specular = other.specular;
		specularExponent = other.specularExponent;
		reflectance = other.reflectance;
		transmittance = other.transmittance;
	}
	
public:
	virtual Radiance diffuseColor (double /*u*/, double /*v*/)
	{
		return m_diffuseColor;
	}
	
	virtual Radiance specularColor (double /*u*/, double /*v*/)
	{
		return m_specularColor;
	}
};

#endif // __COLOR_MATERIAL_H__
