#ifndef __INTERSECTION_DATA_3D_H__
#define __INTERSECTION_DATA_3D_H__

#include <armadillo>

#include "Object3D.h"

using namespace arma;


/**
 * @class 	IntersectionData3D
 * @author 	Sarathi Hansen
 *
 * An IntersectionData3D object contains the object with which the
 * intersection took place, the position of the intersection, the surface
 * normal of the object at the intersection position, the incoming vector
 * that intersected with the object, and the distance along the incoming
 * ray at which the intersection took place.
 * Also included is the uv-coordinates for texture mapping the intersection.
 */

class IntersectionData3D
{
public:
	const vec3 position;
	const vec3 normal;
	const Ray3D incoming;
	const double omega;
	const double u;
	const double v;
	const Object3D * object;
	
	IntersectionData3D (const vec3 & position,
						const vec3 & normal,
						const Ray3D & incoming,
						double omega,
						double u,
						double v,
						const Object3D * object):
	position (position),
	normal (normalise (normal)),
	incoming (incoming),
	omega (omega),
	u (u),
	v (v),
	object (object) {}
};

#endif // __INTERSECTION_DATA_3D_H__
