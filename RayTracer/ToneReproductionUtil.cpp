#include "ToneReproductionUtil.h"
#include <cmath>


double ToneReproductionUtil::getAbsoluteLuminance (const Radiance & radiance)
{
	return (0.27*radiance.r + 0.67*radiance.g + 0.06*radiance.b);
}

double ToneReproductionUtil::getLogAverageLuminance (const vector<Radiance> & pixelRadianceValues)
{
	double logAverageLuminance = 0;
	
	for (const Radiance & radiance : pixelRadianceValues)
	{
		logAverageLuminance += log (0.0001 + ToneReproductionUtil::getAbsoluteLuminance (radiance));
	}
	
	logAverageLuminance /= pixelRadianceValues.size ();
	
	return exp (logAverageLuminance);
}

double ToneReproductionUtil::getMaxLuminance (const vector<Radiance> & pixelRadianceValues)
{
	double maxLuminance = 0;
	
	for (const Radiance & radiance : pixelRadianceValues)
	{
		double luminance = ToneReproductionUtil::getAbsoluteLuminance (radiance);
		if (luminance > maxLuminance)
			maxLuminance = luminance;
	}
	
	return maxLuminance;
}

void ToneReproductionUtil::modelColorAppearance (Radiance & radiance, double ldmax)
{
	radiance.clamp (0, ldmax);
	radiance /= ldmax;
	radiance *= 0xFF;
}

ToneReproductionUtil::model ToneReproductionUtil::linearModel (double ldmax)
{
	return [ldmax] (vector<Radiance> & pixelRadianceValues) -> void
	{
		for (Radiance & radiance : pixelRadianceValues)
		{
			modelColorAppearance (radiance, ldmax);
		}
	};
}

ToneReproductionUtil::model ToneReproductionUtil::wardModel (double ldmax)
{
	return [ldmax] (vector<Radiance> & pixelRadianceValues) -> void
	{
		double logAverageLuminance = ToneReproductionUtil::getLogAverageLuminance (pixelRadianceValues);
		double sf = pow ((1.219 + pow (ldmax / 2.0, 0.4)) / (1.219 + pow (logAverageLuminance, 0.4)), 2.5) / ldmax;
		
		for (Radiance & radiance : pixelRadianceValues)
		{
			radiance *= sf;
			
			modelColorAppearance (radiance, ldmax);
		}
	};
}

ToneReproductionUtil::model ToneReproductionUtil::reinhardModel (double ldmax, double key /*= 0.18 */)
{
	return [ldmax, key] (vector<Radiance> & pixelRadianceValues) -> void
	{
		double logAverageLuminance = ToneReproductionUtil::getLogAverageLuminance (pixelRadianceValues);
		double sf = key / logAverageLuminance;
		
		for (Radiance & radiance : pixelRadianceValues)
		{
			Radiance scaled = radiance * sf;
			Radiance reflectance = scaled / (scaled + 1);
			radiance = reflectance * ldmax;
			
			modelColorAppearance (radiance, ldmax);
		}
	};
}

ToneReproductionUtil::model ToneReproductionUtil::adaptiveLogarithmicModel (double ldmax, double b /*= 0.85 */)
{
	return [ldmax, b] (vector<Radiance> & pixelRadianceValues) -> void
	{
		double logAverageLuminance = ToneReproductionUtil::getLogAverageLuminance (pixelRadianceValues);
		double maxLuminance = ToneReproductionUtil::getMaxLuminance (pixelRadianceValues) / logAverageLuminance;
		
		
		double maxVal = 0;
		
		for (const Radiance & radiance : pixelRadianceValues)
		{
			if (radiance.r > maxVal)
				maxVal = radiance.r;
			if (radiance.g > maxVal)
				maxVal = radiance.g;
			if (radiance.b > maxVal)
				maxVal = radiance.b;
		}
		
		for (Radiance & radiance : pixelRadianceValues)
		{
			double lw = ToneReproductionUtil::getAbsoluteLuminance (radiance) / logAverageLuminance;
			double ld = ((log (lw + 1) / log (2 + pow (lw/maxLuminance, log (b)/log (0.5)) * 8)) / log10 (maxLuminance + 1));
			
			radiance /= maxVal;
			radiance *= ldmax;
			radiance *= ld;
			
			modelColorAppearance (radiance, ldmax);
		}
	};
}
