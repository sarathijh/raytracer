#ifndef __MATERIAL_BASE_H__
#define __MATERIAL_BASE_H__

#include "Radiance.h"


/**
 * @class  Material
 * @author Sarathi Hansen
 *
 * Represents a base material with all the properties that
 * are common to all types of materials.
 */

class Material
{
public:
	double diffuse;
	double specular;
	double specularExponent;
	double reflectance;
	double transmittance;
	double indexOfRefraction;
	
public:
	Material (double diffuse = 1.0,
			  double specular = 0.0,
			  double specularExponent = 0.0,
			  double reflectance = 0.0,
			  double transmittance = 0.0,
			  double indexOfRefraction = 1.0):
	diffuse (diffuse),
	specular (specular),
	specularExponent (specularExponent),
	reflectance (reflectance),
	transmittance (transmittance),
	indexOfRefraction (indexOfRefraction) {}
	
	void operator = (const Material & other)
	{
		diffuse = other.diffuse;
		specular = other.specular;
		specularExponent = other.specularExponent;
		reflectance = other.reflectance;
		transmittance = other.transmittance;
	}
	
public:
	virtual Radiance diffuseColor (double u, double v) = 0;
	virtual Radiance specularColor (double u, double v) = 0;
};

#endif // __MATERIAL_BASE_H__
