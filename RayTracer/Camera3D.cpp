#include "Camera3D.h"
#include "ToneReproductionUtil.h"

#include <thread>


// Set the number of threads to the concurrency capacity of the hardware
const unsigned int Camera3D::nthreads = std::thread::hardware_concurrency ();


Camera3D::Camera3D (const vec3 & position,
					const vec3 & lookat,
					const vec3 & up,
					const Camera3D::ViewPlane & viewPlane):
m_position (position),
m_lookat (lookat),
m_up (up),
m_viewPlane (viewPlane),
m_viewMatrixDirty (true)
{
	
}
	
mat44 Camera3D::viewMatrix ()
{
	// If the view matrix needs to be updated, then update and cache it
	if (m_viewMatrixDirty)
	{
		vec3 z = normalise (m_position - m_lookat);
		vec3 x = normalise (cross (m_up, z));
		vec3 y = normalise (cross (z, x));
		
		m_viewMatrix = mat44 {
			x(0),                 y(0),                 z(0),                 0,
			x(1),                 y(1),                 z(1),                 0,
			x(2),                 y(2),                 z(2),                 0,
			-dot (x, m_position), -dot (y, m_position), -dot (z, m_position), 1};
		
		m_viewMatrixDirty = false;
	}
	
	// Return the cached view matrix
	return m_viewMatrix;
}

Ray3D Camera3D::getRayThrough (double x, double y)
{
	double vx = -m_viewPlane.width/2.0 + x;
	double vy = -m_viewPlane.height/2.0 + y;
	double vz = -m_viewPlane.z;
	
	return Ray3D (vec3 {vx, -vy, vz}, vec3 {0.0, 0.0, 0.0}, 1.0);
}

void Camera3D::render (const string & filename,
					   Scene3D * scene,
					   IlluminationUtil::model illuminationModel /*= IlluminationUtil::phongModel*/,
					   ToneReproductionUtil::model toneReproductionModel /*= ToneReproductionUtil::linear*/,
					   SamplingUtil::sampler sampler /*= SamplingUtil::center*/)
{
	this->render (filename, scene, [scene, illuminationModel] (const IntersectionData3D * intersectionData) -> Radiance
				  {
					  return IlluminationUtil::illuminate (intersectionData,
														   scene,
														   illuminationModel) * intersectionData->incoming.intensity;
				  }, toneReproductionModel, sampler);
}

void Camera3D::render (const string & filename,
					   Scene3D * scene,
					   Camera3D::shader surfaceShader,
					   ToneReproductionUtil::model toneReproductionModel /*= ToneReproductionUtil::linear*/,
					   SamplingUtil::sampler sampler /*= SamplingUtil::center*/)
{
	// Create a blank image with the dimensions of the camera's viewing plane.
	bitmap_image bitmap (m_viewPlane.width, m_viewPlane.height);
	
	// Transform all the lights and objects in the scene to camera space.
	scene->transform (this->viewMatrix ());
	
	vector<Radiance> pixelRadianceValues (m_viewPlane.width * m_viewPlane.height);
	
	std::vector<std::thread> threads;
	
	for (int i = 0; i < nthreads; ++i)
	{
		threads.push_back (std::thread ([&sampler, &surfaceShader, &pixelRadianceValues, scene, this] (int i) -> void
										{
											for (int y = 0; y < m_viewPlane.height; ++y)
											{
												for (int x = i; x < m_viewPlane.width; x += nthreads)
												{
													// Run the recursive ray tracing algorithm through the given
													// sampler to calculate the radiance for the current pixel.
													Radiance radiance = sampler ([x, y, surfaceShader, scene, this] (double sampleX, double sampleY) -> Radiance
																				 {
																					 return scene->traceRay (this->getRayThrough (x + sampleX, y + sampleY), 10, surfaceShader);
																				 });
													
													pixelRadianceValues [y * m_viewPlane.width + x] = radiance;
												}
											}
										}, i));
	}
	for (int i = 0; i < nthreads; ++i)
	{
		threads [i].join ();
	}
	
	/*for (int y = 0; y < m_viewPlane.height; ++y)
	{
		for (int x = 0; x < m_viewPlane.width; ++x)
		{
			// Run the recursive ray tracing algorithm through the given
			// sampler to calculate the radiance for the current pixel.
			Radiance radiance = sampler ([x, y, surfaceShader, scene, this] (double sampleX, double sampleY) -> Radiance
										 {
											 return scene->traceRay (this->getRayThrough (x + sampleX, y + sampleY), 10, surfaceShader);
										 });
			
			pixelRadianceValues [y * m_viewPlane.width + x] = radiance;
		}
	}*/
	
	toneReproductionModel (pixelRadianceValues);
	
	for (int y = 0; y < m_viewPlane.height; ++y)
	{
		for (int x = 0; x < m_viewPlane.width; ++x)
		{
			Radiance radiance = pixelRadianceValues [y * m_viewPlane.width + x];
			bitmap.set_pixel (x, y, radiance.r, radiance.g, radiance.b);
		}
	}
	
	bitmap.save_image (filename);
}
