#include "Scene3D.h"


Scene3D::Scene3D (const Radiance & backgroundRadiance):
m_backgroundRadiance (backgroundRadiance)
{
	
}

void Scene3D::addObject (Object3D * object)
{
	m_objects.push_back (object);
}

void Scene3D::addLight (Light3D * light)
{
	m_lights.push_back (light);
}

void Scene3D::transform (const mat44 & matrix)
{
	for (Object3D * object : m_objects)
		object->transform (matrix);
	
	for (Light3D * light : m_lights)
		light->transform (matrix);
}

bool Scene3D::rayHitsLight (const Ray3D & ray, Light3D * light) const
{
	vec3 shadowRay = light->position () - ray.position;
	double sdist = sqrt (shadowRay(0)*shadowRay(0) + shadowRay(1)*shadowRay(1) + shadowRay(2)*shadowRay(2));
	shadowRay = normalise (shadowRay);
	
	bool lightHits = true;
	
	const IntersectionData3D * closestIntersectionData = closestIntersectionWithRay (ray);
	
	if (closestIntersectionData)
		lightHits = (closestIntersectionData->omega > sdist);
	
	return lightHits;
}

const IntersectionData3D * Scene3D::closestIntersectionWithRay (const Ray3D & ray) const
{
	double minDistance = numeric_limits<double>::max ();
	const IntersectionData3D * closestIntersectionData = NULL;
	
	// Check for an intersection with each object in the scene.
	for (Object3D * object : m_objects)
	{
		// Check for an intersection with the current object.
		const IntersectionData3D * intersectionData = object->rayIntersection (ray);
		
		if (intersectionData != NULL)
		{
			// The distance along the ray at which the intersection takes place.
			double distance = intersectionData->omega;
			
			// If the intersection takes place in front of the camera and is closer than
			// the current closest intersection, then update the closest intersection.
			if (distance > 0 && distance < minDistance)
			{
				minDistance = distance;
				closestIntersectionData = intersectionData;
			}
		}
	}
	
	return closestIntersectionData;
}

Radiance Scene3D::traceRay (const Ray3D & ray, int maxDepth, std::function<Radiance (const IntersectionData3D * intersectionData)> shader) const
{
	if (maxDepth <= 0)
		return 0;
	
	const IntersectionData3D * closestIntersectionData = closestIntersectionWithRay (ray);
	
	if (closestIntersectionData == NULL)
		return this->backgroundRadiance () * ray.intensity;
	
	Radiance radiance = shader (closestIntersectionData);
	
	if (maxDepth > 1)
	{
		Material * material = closestIntersectionData->object->material ();
		
		if (material->reflectance > 0)
		{
			Ray3D reflectedRay = Scene3D::getReflectedRayFromIntersectionData (closestIntersectionData);
			radiance += this->traceRay (reflectedRay, maxDepth - 1, shader);
		}
		
		if (material->transmittance > 0)
		{
			Ray3D transmittedRay = Scene3D::getTransmittedRayFromIntersectionData (closestIntersectionData);
			radiance += this->traceRay (transmittedRay, maxDepth - 1, shader);
		}
	}
	
	return radiance;
}

Ray3D Scene3D::getReflectedRayFromIntersectionData (const IntersectionData3D * intersectionData)
{
	vec3 S = normalise (intersectionData->incoming.direction);
	vec3 N = normalise (intersectionData->normal);
	vec3 R = normalise (S - 2 * dot (S, N) * N);
	
	return Ray3D (R, intersectionData->position, intersectionData->object->material ()->reflectance);
}

Ray3D Scene3D::getTransmittedRayFromIntersectionData (const IntersectionData3D * intersectionData)
{
	const Material * material = intersectionData->object->material ();
	
	// The indicies of refraction for the outside and inside of the object
	double n1 = 1.00;
	double n2 = material->indexOfRefraction;
	
	// the incoming ray
	vec3 V = normalise (intersectionData->incoming.direction);
	// the surface normal
	vec3 N = normalise (intersectionData->normal);
	
	bool inside = (dot (N, V) < 0);
	
	if (inside)
	{
		// If we are inside the object, then flip the normal, and swap the indicies of refraction
		N = -N;
		n1 = material->indexOfRefraction;
		n2 = 1.00;
	}
	
	double n = n1 / n2;
	double c1 = -dot (N, V);
	double c2 = 1.0f - n*n * (1.0f - c1*c1);
	
	if (c2 < 0)
	{
		// If we are inside the object and the discriminant is negative, then we have total internal reflection.
		// Therefore, return the reflected ray instead of the refracted ray.
		vec3 R = normalise (V - 2 * dot (V, N) * N);
		return Ray3D (R, intersectionData->position, intersectionData->object->material ()->transmittance);
	}
	
	// Calculate the final refracted ray
	vec3 t = n * V + N * c1 + sqrt (c2) * N;
	
	return Ray3D (t, intersectionData->position, material->transmittance);
}
