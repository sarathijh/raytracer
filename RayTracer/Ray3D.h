#ifndef __RAY_3D_H__
#define __RAY_3D_H__

#include <armadillo>

using namespace arma;


/**
 * @class 	Ray3D
 * @author 	Sarathi Hansen
 *
 * A Ray3D consists of a position and a direction vector.
 */

class Ray3D
{
public:
	const vec3 direction;
	const vec3 position;
	const double intensity;
	
public:
	Ray3D (const vec3 & direction,
		   const vec3 & position,
		   double intensity):
	direction (normalise (direction)),
	position (position),
	intensity (intensity) {}
	
	Ray3D (const Ray3D & other):
	direction (other.direction),
	position (other.position),
	intensity (other.intensity) {}
};

#endif // __RAY_3D_H__
