#ifndef __PIVOT_3D_H__
#define __PIVOT_3D_H__

#include <armadillo>

using namespace arma;


/**
 * @class	Pivot3D
 * @author	Sarathi Hansen
 *
 * The Pivot3D class represents a position in 3D space that
 * can be transformed and placed into a Scene3D.
 */

class Pivot3D
{
protected:
	vec3 m_position;
	
	// Getters & Setters
public:
	const vec3 & position () const { return m_position; }
	void setPosition (const vec3 & value) { m_position = value; }
	
	// Constructor(s) & Destructor
public:
	/**
	 * Creates a new pivot at the given position.
	 */
	Pivot3D (const vec3 & position);
	
public:
	/**
	 * Transforms the pivot position using the given transformation matrix.
	 */
	virtual void transform (const mat44 & matrix);
};

#endif // __PIVOT_3D_H__
