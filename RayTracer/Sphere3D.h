#ifndef __RayTracer__Sphere3D__
#define __RayTracer__Sphere3D__

#include <iostream>

#include "Object3D.h"
#include <limits>

using namespace std;
using namespace arma;


/**
 * @class 	Sphere3D
 * @author 	Sarathi Hansen
 *
 * The Sphere3D class defines the geometry for a sphere
 * to be transformed and placed into a Scene3D.
 */

class Sphere3D : public Object3D
{
public:
	double radius;
	
public:
	Sphere3D (const vec3 & position,
			  double radius,
			  Material * material):
	Object3D (position, material),
	radius (radius) {}
	
	virtual const IntersectionData3D * rayIntersection (const Ray3D & ray, bool print = false) const
	{
		vec3 centerToRay = ray.position - m_position;
		
		double B = 2 * dot (ray.direction, centerToRay);
		double C = dot (centerToRay, centerToRay) - (radius*radius);
		
		double discriminant = B*B - 4*C;
		double omega = numeric_limits<double>::max ();
		
		if (discriminant == 0)
		{
			omega = -B / 2;
		}
		else if (discriminant > 0)
		{
			double sqrtDisc = sqrt (discriminant);
			double omegaAdd = (-B + sqrtDisc) / 2;
			double omegaSub = (-B - sqrtDisc) / 2;
			
			omega = Object3D::findSmallestOmega (std::vector<double> {omegaAdd, omegaSub});
		}
		
		if (omega == numeric_limits<double>::max ())
			return NULL;
		
		vec3 point = ray.position + ray.direction * omega;
		vec3 normal = normalise (point - m_position);
		
		vec3 dHat = point - m_position;
		double u = (0.5 + atan2 (dHat(0), dHat(2)) / (2*3.14159));
		double v = (0.5 + atan2 (dHat(1), dHat(2)) / (2*3.14159));
		
		return new IntersectionData3D (point, normal, ray, omega, u, v, this);
	}
};

#endif /* defined(__RayTracer__Sphere3D__) */
