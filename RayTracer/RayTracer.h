#ifndef __RAY_TRACER_H__
#define __RAY_TRACER_H__

#include <armadillo>

#include "Scene3D.h"
#include "Camera3D.h"
#include "Light3D.h"
#include "Radiance.h"
#include "Sphere3D.h"
#include "Plane3D.h"
#include "ColorMaterial.h"
#include "SamplingUtil.h"
#include "CheckeredMaterial.h"
#include "TexturedMaterial.h"
#include "IlluminationUtil.h"

using namespace arma;


/**
 * @class	RayTracer
 * @author	Sarathi Hansen
 *
 * The main application class for the ray tracer.
 * This class sets up and renders the scene.
 */

class RayTracer
{
public:
	RayTracer ()
	{
		// Create the scene
		Scene3D scene (Radiance (0.086, 0.655, 0.827));
		
		// Add a light
		Light3D light (vec3 {-17, 100, 100}, Radiance (1.0, 1.0, 1.0)*10000);
		
		scene.addLight (&light);
		
		/*ColorMaterial translucent (radiance (1.0, 1.0, 1.0), // Diffuse color
								   radiance (1.0, 1.0, 1.0), // Specular color
								   0.5, // Diffuse contribution
								   0.5, // Specular contribution
								   40,  // Specular exponent
								   0.0, // Reflectance
								   1.0, // Transmittance
								   1.05 // Index of refraction
								   );*/
		
		// Create the floor and two spheres
		Material * translucentMaterial = new ColorMaterial (Radiance (1.0, 1.0, 1.0), // Diffuse color
															Radiance (1.0, 1.0, 1.0), // Specular color
															0.5,   // Diffuse contribution
															0.5,   // Specular contribution
															100,   // Specular exponent
															0.0,   // Reflectance
															1.0,   // Transmittance
															1.05); // Index of refraction
		
		Material * reflectiveMaterial = new ColorMaterial (Radiance (1.0, 1.0, 1.0), // Diffuse color
														   Radiance (1.0, 1.0, 1.0), // Specular color
														   0.5,  // Diffuse contribution
														   0.5,  // Specular contribution
														   100,  // Specular exponent
														   1.0); // Reflectance
		
		Material * checkeredMaterial = new CheckeredMaterial (Radiance (1.0, 0.0, 0.0), // First color
															  Radiance (1.0, 1.0, 0.0), // Second color
															  16, 20); // Number of squares (width x height)
		
		Sphere3D leftSphere  (vec3 {-12.5, 10.2, 49.0}, 4.0, translucentMaterial);
		Sphere3D rightSphere (vec3 { -6.8,  6.9, 42.0}, 3.6, reflectiveMaterial);
		Plane3D  floor       (vec3 {  0.0,  0.0,  0.0}, 50, 100, vec3 {0.0, 1.0, 0.0}, checkeredMaterial);
		
		Material * skyBoxMaterial = new TexturedMaterial ("/Users/sarathi/Downloads/skybox.bmp");
		
		Sphere3D skyBox (vec3 {0, 0, 0}, 150, skyBoxMaterial);
		scene.addObject (&skyBox);
		
		scene.addObject (&leftSphere);
		scene.addObject (&rightSphere);
		scene.addObject (&floor);
		
		// Create a camera
		Camera3D camera (vec3 {-11.5, 10.0, 69.0},
						 vec3 {-12.5,  9.0,  0.0},
						 vec3 {  0.0,  1.0,  0.0},
						 Camera3D::ViewPlane {545, 420, 500});
		
		// Render the scene using the camera
		camera.render ("/Users/sarathi/Downloads/render.bmp",
					   &scene,
					   IlluminationUtil::phongModel,
					   ToneReproductionUtil::adaptiveLogarithmicModel (86),
					   SamplingUtil::center);
	}
};

#endif // __RAY_TRACER_H__
