#include "SamplingUtil.h"
#include <vector>

using namespace std;


Radiance SamplingUtil::center (std::function<Radiance (double, double)> callback)
{
	return callback (0.5, 0.5);
}

SamplingUtil::sampler SamplingUtil::grid (double samples)
{
	return [samples] (std::function<Radiance (double, double)> callback) -> Radiance
	{
		Radiance radiance;
		vector<double> samplePoints;
		
		for (int i = 0; i < samples; ++i)
			samplePoints.push_back ((double)i / (double)(samples-1));
		
		for (double sampleY : samplePoints)
		{
			for (double sampleX : samplePoints)
			{
				radiance += callback (sampleX, sampleY);
			}
		}
		
		radiance /= samples*samples;
		return radiance;
	};
}
